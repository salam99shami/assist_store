import 'package:shop/utils/enums/user_type_enum.dart';

class UserModel {
  int id;
  String name;
  String email;
  String createdAt;
  String updatedAt;
  String phone;
  int verifiedCode;
  String? city;
  String? imgPath;
  String? address;
  double? latitude;
  double? longtude; //flutter map
  String? notes;
  UserTypeEnum userType = UserTypeEnum.customer;
  // String password;

  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.verifiedCode,
    required this.createdAt,
    required this.updatedAt,
    this.imgPath,
    this.city,
    this.address,
    this.latitude,
    this.longtude,
    this.notes,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"] as int,
        name: json["name"] as String,
        email: json["email"] as String,
        phone: json["phone"] as String,
        verifiedCode: json["verified_code"] as int,
        createdAt: json["created_at"] as String,
        updatedAt: json["updated_at"] as String,
        imgPath: json["imgPath"] == null ? "" : json["imgPath"] as String,
        city: json["city"] == null ? "" : json["city"] as String,
        address: json["address"] == null ? "" : json["address"] as String,
        latitude: json["latitude"] == null ? 0 : json["latitude"] as double,
        longtude: json["longtude"] == null ? 0 : json["longtude"] as double,
        notes: json["notes"] == null ? "" : json["city"] as String,
        // password: json["password"] as String,
        // userType: UserTypeEnum.values[json["userType"] as int] == null
        //     ? UserTypeEnum.customer
        //     : UserTypeEnum.values[json["userType"] as int],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "imgPath": imgPath,
        // "password": password,
        "phone": phone,
        // "userType": userType==null?0:userType.index!,
        "verifiedCode": verifiedCode,
        "city": city,
        "address": address,
        "latitude": latitude,
        "longtude": longtude,
        "notes": notes,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
