import 'package:shop/models/image_model.dart';

class ProductModel {
  int id;
  String name;
  String description;
  List<ImageModel>? images;
  double price;
  double? price1;
  double? price2;
  double? price3;
  double? price4;
  double? costPrice;
  bool isAvailable;
  bool? isFavorite;
  int categoryId;
  String? notes;
  double TVA;

  ProductModel({
    required this.id,
    required this.name,
    required this.price,
    required this.categoryId,
    required this.isAvailable,
    required this.description,
    required this.TVA,
    this.isFavorite,
    this.images,
    this.costPrice,
    this.price1,
    this.price2,
    this.price3,
    this.price4,
    this.notes,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    final List<ImageModel> tempImages = [];
    for (var image in json['images']) {
      tempImages.add(ImageModel.fromJson(image));
    }
    return ProductModel(
      id: json["id"] as int,
      name: json["name"] as String,
      description: json["description"] as String,
      images: tempImages,
      categoryId: json["categoryId"] as int,
      isAvailable: json["isAvailable"] as bool,
      price: json["price"] as double,
      costPrice: json["costPrice"] == null ? null : json["costPrice"] as double,
      isFavorite:
          json["isFavorite"] == null ? null : json["isFavorite"] as bool,
      price1: json["price1"] == null ? null : json["price1"] as double,
      price2: json["price2"] == null ? null : json["price2"] as double,
      price3: json["price3"] == null ? null : json["price3"] as double,
      price4: json["price4"] == null ? null : json["price4"] as double,
      notes: json["notes"] == null ? null : json["notes"] as String,
      TVA: json["TVA"] as double,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "categoryId": categoryId,
        "isAvailable": isAvailable,
        "isFavorite": isFavorite,
        "price": price,
        "price1": price1,
        "price2": price2,
        "price3": price3,
        "price4": price4,
        "costPrice": costPrice,
        "notes": notes,
        "images": images,
        "TVA": TVA,
      };
}
