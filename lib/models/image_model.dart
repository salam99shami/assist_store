class ImageModel {
  int id;
  String image;

  ImageModel({
    required this.id,
    required this.image,
  });

  factory ImageModel.fromJson(Map<String, dynamic> json) => ImageModel(
        id: json["id"] as int,
        image: json["image"] as String,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
      };
}
