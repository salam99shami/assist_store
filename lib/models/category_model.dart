class CategoryModel {
  int id;
  String name;
  String? description;
  String? notes;
  String? iconPath;

  CategoryModel({
    required this.id,
    required this.name,
    this.description,
    this.iconPath,
    this.notes,
  });

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        id: json["id"] as int,
        name: json["name"] as String,
        description:
            json["description"] == null ? null : json["description"] as String,
        iconPath: json["iconPath"] == null ? null : json["iconPath"] as String,
        notes: json["notes"] == null ? null : json["notes"] as String,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "iconPath": iconPath,
        "notes": notes,
      };
}
