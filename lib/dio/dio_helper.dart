import 'package:dio/dio.dart';

class DioHelper {
  static Dio? dio;
  static const String baseUrl = "http://192.168.1.28:8000/api/";
  static init({String token = ""}) {
    dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        validateStatus: (status) {
          return status! <= 500;
        },
        receiveDataWhenStatusError: true,
        headers: {
          "authorization": "Bearer $token",
          "Accept": "application/json"
        },
      ),
    );
  }
}
/// interceprors