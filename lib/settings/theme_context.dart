import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(
  colorScheme: const ColorScheme(
    brightness: Brightness.light,
    // primary: Color(0xFF8758FF),
    primary: Color(0xFF197878),
    // primary: Color(0xFF89c4c1),
    onPrimary: Color(0xFFF2F2F2),
    // secondary: Color(0xFF5CB8E4),
    // secondary: Color(0xFF1c2243),
    // secondary: Color(0xFF197878),
    secondary: Color(0xFF89c4c1),
    onSecondary: Color(0xFFF2F2F2),
    error: Color.fromARGB(255, 218, 65, 54),
    onError: Color(0xFF181818),
    background: Color(0xFF181818),
    onBackground: Color(0xFF181818),
    surface: Colors.white,
    onSurface: Color(0xFF181818),
  ),
);

ThemeData darkTheme = ThemeData(
  colorScheme: const ColorScheme(
    brightness: Brightness.dark,
    // primary: Color(0xFF8758FF),
    primary: Color(0xFF197878),
    onPrimary: Color(0xFF181818),
    // secondary: Color(0xFF5CB8E4),
    secondary: Color(0xFF89c4c1),
    onSecondary: Color(0xFF181818),
    error: Color.fromARGB(255, 218, 65, 54),
    onError: Color(0xFFF2F2F2),
    background: Color(0xFFF2F2F2),
    onBackground: Color(0xFFF2F2F2),
    surface: Color(0xFF181818),
    onSurface: Color(0xFFF2F2F2),
  ),
);
