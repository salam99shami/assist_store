import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/dashboard_bottom_navigator_bar.dart';
import 'package:shop/components/dashboard_drawer.dart';

class DashboardManageAccountPage extends StatelessWidget {
  DashboardManageAccountPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        homePageCubit.searchUserList = homePageCubit.productList;
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            drawer: const DashboardDrawer(),
            appBar: AppBar(
              title: Text(languageMap["Manage Accounts"]!),
            ),
            body: homePageCubit.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return languageMap["this field is required"];
                                }
                                return null;
                              },
                              onChanged: (value) =>
                                  homePageCubit.searchUserInDashboard(value),
                              controller:
                                  homePageCubit.searchUserDashboardController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.search),
                                label: Text(languageMap["Search"]!),
                                border: const OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 8),
                        ],
                      ),
                    ),
                  ),
            bottomNavigationBar: const DashboardBottomNavigatorBar(),
          ),
        );
      },
    );
  }
}
