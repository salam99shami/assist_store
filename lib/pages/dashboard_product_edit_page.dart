import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/dashboard_bottom_navigator_bar.dart';
import 'package:shop/components/dashboard_drawer.dart';
import 'package:shop/components/dropdown_component.dart';
import 'package:shop/models/category_model.dart';

class DashboardProductEditPage extends StatefulWidget {
  DashboardProductEditPage({super.key});

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  State<DashboardProductEditPage> createState() =>
      _DashboardProductEditPageState();
}

class _DashboardProductEditPageState extends State<DashboardProductEditPage> {
  List<File>? imagesList;

  Future pickProductImage() async {
    try {
      final List<XFile> images = await ImagePicker().pickMultiImage();
      if (images == null) return;
      setState(() {
        for (var image in images) {
          imagesList!.add(File(image.path));
        }
      });
    } catch (e) {
      print("Failed to pick images : $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: homePageCubit.currentProductId != null
                  ? Text(languageMap["Edit Product Page"]!)
                  : Text(languageMap["Add Product Page"]!),
            ),
            drawer: const DashboardDrawer(),
            body: SingleChildScrollView(
              child: Form(
                key: widget.formKey,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: 400,
                            height: 250,
                            decoration: BoxDecoration(
                              image: imagesList == null
                                  ? const DecorationImage(
                                      image: AssetImage(
                                        "assets/images/default.jpg",
                                      ),
                                      fit: BoxFit.fill,
                                    )
                                  : DecorationImage(
                                      image: FileImage(
                                        imagesList![0],
                                      ),
                                      fit: BoxFit.fill,
                                    ),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(5),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                              top: 200,
                              left: 270,
                            ),
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(50),
                              ),
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  Theme.of(context).colorScheme.primary,
                                  Theme.of(context).colorScheme.secondary,
                                ],
                              ),
                            ),
                            child: IconButton(
                              padding: const EdgeInsets.all(6),
                              iconSize: 25,
                              onPressed: () {
                                pickProductImage();
                              },
                              icon: const Icon(Icons.add),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 8),
                      TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return languageMap["this field is required"];
                          }
                          return null;
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: homePageCubit.productNameController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.shopping_cart_checkout),
                          label: Text(languageMap["Name"]!),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(height: 8),
                      TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return languageMap["this field is required"];
                          }
                          return null;
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: homePageCubit.productPriceController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.price_change),
                          label: Text(languageMap["Price"]!),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(height: 8),
                      TextFormField(
                        controller: homePageCubit.productDescriptionController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.description),
                          label: Text(languageMap["Description"]!),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(height: 8),
                      TextFormField(
                        controller: homePageCubit.productNotesController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.notes),
                          label: Text(languageMap["Notes"]!),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(height: 8),
                      DropDownComponent<CategoryModel>(
                        value: homePageCubit.categorySelectedModel,
                        nullable: true,
                        items: homePageCubit.categoriesList,
                        itemsBuilder: (obj) => Text(obj!.name),
                        labeltext: languageMap["Category"],
                        onChanged: (newValue) {
                          homePageCubit.changeProductCategory(newValue);
                        },
                      ),
                      const SizedBox(height: 8),
                      Container(
                        width: MediaQuery.of(context).size.width * 1,
                        height: 55,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              Theme.of(context).colorScheme.primary,
                              Theme.of(context).colorScheme.secondary,
                            ],
                          ),
                        ),
                        child: ElevatedButton(
                          onPressed: () async {
                            if (widget.formKey.currentState!.validate()) {
                              Navigator.of(context).pop();
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                          ),
                          child: Text(languageMap["Save"]!),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
