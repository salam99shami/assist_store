import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/app_bottom_navigator_bar.dart';
import 'package:shop/components/app_drawer.dart';
import 'package:shop/components/category_component.dart';
import 'package:shop/models/category_model.dart';

class SubCategoriesPage extends StatelessWidget {
  SubCategoriesPage({
    super.key,
    required this.categoryModel,
  });

  CategoryModel categoryModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: Text(languageMap["Sub Categories Page"]!),
            ),
            drawer: const AppDrawer(),
            body: SingleChildScrollView(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const SizedBox(height: 8),
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width * 1,
                      child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          childAspectRatio: 1.1,
                          // mainAxisSpacing: 8,
                          // crossAxisSpacing: 8,
                        ),
                        itemBuilder: ((context, index) {
                          return CategoryComponent(category: categoryModel);
                        }),
                        itemCount: homePageCubit.categoriesList.length,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
