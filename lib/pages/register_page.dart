import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/pages/verifiey_code_page.dart';

import 'login_page.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({super.key});

  bool isValidEmail(String email) {
    return RegExp(
      r"^[a-zA-Z.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+$",
    ).hasMatch(email);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                languageMap["Sign up"]!,
              ),
            ),
            body: SingleChildScrollView(
              child: Center(
                child: Container(
                  margin: const EdgeInsets.only(
                    top: 12,
                  ),
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.surface,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      const BoxShadow(
                        color: Colors.grey,
                        spreadRadius: 0.1,
                        blurRadius: 1,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 12,
                      right: 12,
                      top: 20,
                      bottom: 20,
                    ),
                    child: Form(
                      key: homePageCubit.registerFormKey,
                      child: Column(
                        children: [
                          Text(
                            languageMap["Join Us"]!,
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 8),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.nameRegister,
                            validator: (value) {
                              if (value == null || value == "") {
                                return languageMap["this field is required"]!;
                              }
                              if (value.length >= 3) {
                                return null;
                              }
                              return languageMap["un-valid name"]!;
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.person),
                              label: Text(
                                languageMap["Name"]!,
                              ),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.text,
                          ),
                          const SizedBox(height: 8),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.emailRegister,
                            validator: (value) {
                              if (value == null || value == "") {
                                return languageMap["this field is required"]!;
                              }
                              if (value.length > 5
                                  // &&
                                  //     isValidEmail(
                                  //         homePageCubit.emailRegister.toString())
                                  ) {
                                return null;
                              }
                              return languageMap['un-valid email'];
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.email),
                              label: Text(languageMap["Email"]!),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),
                          const SizedBox(height: 8),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.passwordRegister,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return languageMap["this field is required"]!;
                              }
                              if (value.length < 8) {
                                return languageMap[
                                    "Password must be 8 characters at least"]!;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.lock),
                              label: Text(languageMap["Password"]!),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.text,
                            obscureText: true,
                          ),
                          const SizedBox(height: 8),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.confirmPasswordRegister,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return languageMap["this field is required"]!;
                              }
                              if (value.length < 8) {
                                return languageMap[
                                    "Password must be 8 characters at least"];
                              }
                              if (value !=
                                  homePageCubit.passwordRegister.text) {
                                return languageMap[
                                    "please confirm the password"]!;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.lock),
                              label: Text(languageMap["Confirm password"]!),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.text,
                            obscureText: true,
                          ),
                          const SizedBox(height: 8),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.phoneRegister,
                            validator: (value) {
                              if (value == null || value == "") {
                                return languageMap["this field is required"]!;
                              }
                              if (value.length == 10) {
                                return null;
                              }
                              return languageMap[
                                  "phone must be 10 characters"]!;
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.phone),
                              label: Text(languageMap["Phone Number"]!),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.phone,
                          ),
                          const SizedBox(height: 8),
                          Container(
                            width: MediaQuery.of(context).size.width * 1,
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  Theme.of(context).colorScheme.primary,
                                  Theme.of(context).colorScheme.secondary,
                                ],
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                if (homePageCubit.registerFormKey.currentState!
                                    .validate()) {
                                  if (await homePageCubit.register()) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          homePageCubit.messagesRegister,
                                        ),
                                      ),
                                    );
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: ((context) =>
                                            VerifieyCodePage()),
                                      ),
                                    );
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          homePageCubit.messagesRegister
                                              .toString(),
                                        ),
                                      ),
                                    );
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shadowColor: Colors.transparent,
                                // shape: RoundedRectangleBorder(
                                //   borderRadius: BorderRadius.circular(25),
                                // ),
                              ),
                              child: Text(languageMap["Sign up"]!),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            bottomSheet: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  languageMap["already have an account ?"]!,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => LoginPage()),
                      ),
                    );
                  },
                  child: Text(
                    languageMap["Login"]!,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
