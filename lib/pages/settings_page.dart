import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/app_drawer.dart';

class SettingsPage extends StatelessWidget {
  SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                languageMap["Settings"]!,
                textDirection: TextDirection.rtl,
              ),
            ),
            drawer: const AppDrawer(),
            body: SingleChildScrollView(
              child: Center(
                child: Container(
                  // height: MediaQuery.of(context).size.height * 0.3,
                  // width: MediaQuery.of(context).size.width * 0.9,
                  margin: const EdgeInsets.only(
                    top: 12,
                  ),
                  // decoration: BoxDecoration(
                  //   color: Theme.of(context).colorScheme.surface,
                  //   borderRadius: BorderRadius.circular(5),
                  //   boxShadow: [
                  //     BoxShadow(
                  //       color: Theme.of(context).colorScheme.primary,
                  //       spreadRadius: 0.1,
                  //       blurRadius: 1,
                  //     ),
                  //   ],
                  // ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(languageMap["Dark Theme"]!),
                          const SizedBox(width: 6),
                          Switch(
                            activeColor:
                                Theme.of(context).colorScheme.secondary,
                            value: homePageCubit.themeMode == ThemeMode.dark,
                            onChanged: (value) {
                              homePageCubit.changeThemeMode(value);
                            },
                          ),
                        ],
                      ),
                      const Divider(color: Colors.grey),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(languageMap["Change Language"]!),
                          const SizedBox(width: 6),
                          Switch(
                            activeColor:
                                Theme.of(context).colorScheme.secondary,
                            value: homePageCubit.language == "EN",
                            onChanged: (value) {
                              homePageCubit.changeLanguage(value);
                            },
                          ),
                        ],
                      ),
                      const Divider(color: Colors.grey),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
