import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/app_bottom_navigator_bar.dart';
import 'package:shop/components/app_drawer.dart';
import 'package:shop/components/products_cart_grid_tile_component.dart';

class MyCartPage extends StatelessWidget {
  const MyCartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            drawer: const AppDrawer(),
            appBar: AppBar(
              title: Text(languageMap["My Cart Page"]!),
            ),
            body: homePageCubit.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : homePageCubit.cartList.isEmpty
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(height: 40),
                          Text(
                            languageMap[
                                "You do not have any product in your cart"]!,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      )
                    : SingleChildScrollView(
                        child: Column(
                          children: [
                            const SizedBox(height: 8),
                            Container(
                              padding: const EdgeInsets.all(2.0),
                              width: MediaQuery.of(context).size.width * 1,
                              height: MediaQuery.of(context).size.height * 1,
                              child: GridView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: homePageCubit.cartList.length,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                ),
                                itemBuilder: (context, index) {
                                  return ProductsCartGridTileComponent(
                                    productModel: homePageCubit.cartList[index],
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
            bottomNavigationBar: const AppBottomNavigatorBar(),
          ),
        );
      },
    );
  }
}
