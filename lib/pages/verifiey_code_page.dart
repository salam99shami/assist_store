import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/text_form_field_component.dart';
import 'package:shop/pages/login_page.dart';

class VerifieyCodePage extends StatelessWidget {
  VerifieyCodePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        String code = homePageCubit.currentUser!.verifiedCode.toString();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                languageMap["Validate of the code"]!,
              ),
            ),
            body: SingleChildScrollView(
              child: Form(
                // key: homePageCubit.verifyFormKey,
                child: Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.4,
                    width: MediaQuery.of(context).size.width * 0.8,
                    margin: const EdgeInsets.only(
                      top: 90,
                    ),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.surface,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        const BoxShadow(
                          color: Colors.grey,
                          spreadRadius: 1,
                          blurRadius: 3,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              // Text("رمز التفعيل :"),
                              Text(
                                homePageCubit.currentUser!.verifiedCode
                                    .toString(),
                                style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  // letterSpacing: 25,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            languageMap["Enter the code :"]!,
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextFormFieldComponent(code: homePageCubit.code1),
                              TextFormFieldComponent(code: homePageCubit.code2),
                              TextFormFieldComponent(code: homePageCubit.code3),
                              TextFormFieldComponent(code: homePageCubit.code4),
                              TextFormFieldComponent(code: homePageCubit.code5),
                              TextFormFieldComponent(code: homePageCubit.code6),
                            ],
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 1,
                            height: 45,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  Theme.of(context).colorScheme.primary,
                                  Theme.of(context).colorScheme.secondary,
                                ],
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                // if (homePageCubit.verifyFormKey.currentState!
                                //     .validate()) {
                                  String codes =
                                      homePageCubit.code1.text.trim() +
                                          homePageCubit.code2.text.trim() +
                                          homePageCubit.code3.text.trim() +
                                          homePageCubit.code4.text.trim() +
                                          homePageCubit.code5.text.trim() +
                                          homePageCubit.code6.text.trim();
                                  // if (code == codes) {
                                  if (await homePageCubit.verifyAccount() &&
                                      codes.length == 6) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          homePageCubit.messagesVerify
                                              .toString(),
                                        ),
                                      ),
                                    );
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: ((context) => LoginPage()),
                                      ),
                                    );
                                  } else {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: ((context) => LoginPage()),
                                      ),
                                    );
                                  }
                                  // }
                                // }
                              },
                              child: Text(languageMap["Validate"]!),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shadowColor: Colors.transparent,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            bottomSheet: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(languageMap["The code did not send ?"]!),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    languageMap["Resend"]!,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
