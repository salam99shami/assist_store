import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/app_bottom_navigator_bar.dart';
import 'package:shop/components/app_drawer.dart';
import 'package:shop/components/categories_list_view_component.dart';
import 'package:shop/components/products_list_view_component.dart';
import 'package:shop/components/products_grid_view_component.dart';
import 'package:shop/components/products_on_sale_list_view_component.dart';
import 'package:shop/pages/login_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        homePageCubit.setOnSaleList();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            drawer: const AppDrawer(),
            appBar: AppBar(
              title: Text(languageMap["Home Page"]!),
              actions: [
                Visibility(
                  // visible: homePageCubit.token == null,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: ((context) => LoginPage()),
                        ),
                      );
                    },
                    icon: const Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                      ),
                      child: const Icon(Icons.login),
                    ),
                  ),
                )
              ],
            ),
            body: homePageCubit.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(height: 8),
                        // Text(
                        //   languageMap["Categories"]!,
                        //   style: const TextStyle(
                        //     fontWeight: FontWeight.bold,
                        //     fontSize: 25,
                        //   ),
                        // ),
                        CategoryListViewComponent(),
                        // const SizedBox(height: 8),
                        // Text(
                        //   languageMap["On Sale"]!,
                        //   style: const TextStyle(
                        //     fontWeight: FontWeight.bold,
                        //     fontSize: 25,
                        //   ),
                        // ),
                        // ProductOnSaleListViewComponent(),
                        // const SizedBox(height: 8),
                        // Ts
                        // const SizedBox(height: 8),
                        ProductsListViewComponent(),
                      ],
                    ),
                  ),
            bottomNavigationBar: const AppBottomNavigatorBar(),
          ),
        );
      },
    );
  }
}
