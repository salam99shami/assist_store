import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/pages/dashboard_page.dart';
import 'package:shop/pages/my_home_page.dart';
import 'package:shop/pages/register_page.dart';
import 'package:shop/utils/enums/user_type_enum.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  bool isValidEmail(String email) {
    return RegExp(
      r"^[a-zA-Z.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+$",
    ).hasMatch(email);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                languageMap["Login"]!,
                textDirection: TextDirection.rtl,
              ),
            ),
            body: SingleChildScrollView(
              child: Form(
                key: homePageCubit.loginFormKey,
                child: Center(
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: 12,
                    ),
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.surface,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        const BoxShadow(
                          color: Colors.grey,
                          spreadRadius: 0.1,
                          blurRadius: 1,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 12.0,
                        right: 12,
                        top: 20,
                        bottom: 20,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            languageMap["Let's get started ?"]!,
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.emailLogin,
                            validator: (value) {
                              if (value == null || value == "") {
                                return languageMap["this field is required"];
                              }
                              if (value.length > 5
                                  // &&
                                  //     isValidEmail(
                                  //         homePageCubit.emailLogin.toString())
                                  ) {
                                return null;
                              }
                              return 'un-valid email';
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.email),
                              label: Text(
                                languageMap["Email"]!,
                              ),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: homePageCubit.passwordLogin,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return languageMap["this field is required"];
                              }
                              if (value.length < 8) {
                                return languageMap[
                                    "Password must be 8 characters at least"];
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.lock),
                              label: Text(languageMap["Password"]!),
                              border: const OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.text,
                            obscureText: true,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 1,
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  Theme.of(context).colorScheme.primary,
                                  Theme.of(context).colorScheme.secondary,
                                ],
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                if (homePageCubit.loginFormKey.currentState!
                                    .validate()) {
                                  if (await homePageCubit.login()) {
                                    if (homePageCubit.currentUser!.userType ==
                                        UserTypeEnum.admin) {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const DashboardPage(),
                                        ),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content: Text(homePageCubit
                                              .messagesLogin
                                              .toString()),
                                        ),
                                      );
                                    } else {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const MyHomePage(),
                                        ),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content: Text(homePageCubit
                                              .messagesLogin
                                              .toString()),
                                        ),
                                      );
                                    }
                                  }
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(homePageCubit.messagesLogin
                                          .toString()),
                                    ),
                                  );
                                }
                              },
                              child: Text(languageMap["Login"]!),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shadowColor: Colors.transparent,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            bottomSheet: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(languageMap["Do not have an account ?"]!),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => RegisterPage()),
                      ),
                    );
                  },
                  child: Text(
                    languageMap["Sign up"]!,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
