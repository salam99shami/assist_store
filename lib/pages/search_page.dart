import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/app_bottom_navigator_bar.dart';
import 'package:shop/components/app_drawer.dart';
import 'package:shop/components/products_grid_view_component.dart';

class SearchPage extends StatelessWidget {
  SearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Directionality(
          textDirection: homePageCubit.language == "AR"
              ? TextDirection.rtl
              : TextDirection.ltr,
          child: Scaffold(
            drawer: const AppDrawer(),
            appBar: AppBar(
              title: Text(languageMap["Search"]!),
            ),
            body: homePageCubit.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Column(
                        children: [
                          const SizedBox(height: 12),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return languageMap["this field is required"];
                                }
                                return null;
                              },
                              onChanged: (value) =>
                                  homePageCubit.searchInApp(value),
                              controller: homePageCubit.searchAppController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.search),
                                label: Text(languageMap["Search"]!),
                                border: const OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 8),
                          ProductGridViewComponent(),
                        ],
                      ),
                    ),
                  ),
            bottomNavigationBar: const AppBottomNavigatorBar(),
          ),
        );
      },
    );
  }
}
