import 'dart:async';
import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/dio/dio_helper.dart';
import 'package:shop/models/category_model.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/models/user_model.dart';
import 'package:shop/pages/dashboard_categories_page.dart';
import 'package:shop/pages/dashboard_page.dart';
import 'package:shop/pages/dashboard_products_page.dart';
import 'package:shop/pages/dashboard_profile_page.dart';
import 'package:shop/pages/dashboard_manage_acounts_page.dart';
import 'package:shop/pages/favorite_page.dart';
import 'package:shop/pages/my_cart_page.dart';
import 'package:shop/pages/my_home_page.dart';
import 'package:shop/pages/profile_page.dart';
import 'package:shop/pages/search_page.dart';
import 'package:shop/settings/language.dart';
import 'package:shop/utils/enums/user_type_enum.dart';

class HomePageCubit extends Cubit<HomePageStates> {
  SharedPreferences sp;
  HomePageCubit(this.sp) : super(HomePageStatesInitalState());

  static HomePageCubit get(context) => BlocProvider.of(context);
  //varibles

  //app
  //app bottomNavigatorBar
  int currentAppPageIndex = 0;
  List appScreens = [
    const MyHomePage(),
    SearchPage(),
    const MyCartPage(),
    FavoritePage(),
    ProfilePage(),
  ];
  //app search
  TextEditingController searchAppController = TextEditingController();
  List<ProductModel> searchInAppList = [];
  //app settings
  String language = "AR";
  bool isLoading = false;
  ThemeMode themeMode = ThemeMode.light;
  UserModel? currentUser;
  bool isAdmin = false;
  //cart
  List<ProductModel> cartList = [];
  //favorite
  List<ProductModel> favoriteList = [];
  //on sale
  List<ProductModel> onSaleList = [];
  //profile
  TextEditingController userCityController = TextEditingController();
  TextEditingController userAddressController = TextEditingController();
  //categories
  // File? categoryImage;
  int? currentCategoryId;
  List<CategoryModel> categoriesList = [];
  TextEditingController categoryNameController = TextEditingController();
  TextEditingController categoryDescriptionController = TextEditingController();
  TextEditingController categoryNotesController = TextEditingController();
  //Products
  int? currentProductId;
  CategoryModel? categorySelectedModel;
  List<ProductModel> productList = [];
  final controller = CarouselController();
  TextEditingController productNameController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  TextEditingController productNotesController = TextEditingController();
  TextEditingController productPriceController = TextEditingController();
  TextEditingController productTVAController = TextEditingController();
  //register
  String messagesRegister = "";
  final GlobalKey<FormState> registerFormKey = GlobalKey<FormState>();
  TextEditingController emailRegister = TextEditingController();
  TextEditingController passwordRegister = TextEditingController();
  TextEditingController confirmPasswordRegister = TextEditingController();
  TextEditingController nameRegister = TextEditingController();
  TextEditingController phoneRegister = TextEditingController();
  //verify
  String messagesVerify = "";
  final GlobalKey<FormState> verifyFormKey = GlobalKey<FormState>();
  TextEditingController code1 = TextEditingController();
  TextEditingController code2 = TextEditingController();
  TextEditingController code3 = TextEditingController();
  TextEditingController code4 = TextEditingController();
  TextEditingController code5 = TextEditingController();
  TextEditingController code6 = TextEditingController();
  //login
  String? token;
  String messagesLogin = "";
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  TextEditingController emailLogin = TextEditingController();
  TextEditingController passwordLogin = TextEditingController();
  //logout
  String messagesLogout = "";

  //dashboard
  //dashboard search
  TextEditingController searchUserDashboardController = TextEditingController();
  TextEditingController searchCategoryDashboardController =
      TextEditingController();
  TextEditingController searchProductDashboardController =
      TextEditingController();
  List<ProductModel> searchUserList = [];
  List<CategoryModel> searchCategoryList = [];
  List<ProductModel> searchProductList = [];
  //dashboard bottomNavigatorBar
  int currentDashboardPageIndex = 0;
  List DashboardScreens = [
    const DashboardPage(),
    const DashboardCategoriesPage(),
    DashboardManageAccountPage(),
    const DashboardProductsPage(),
    DashboardProfilePage(),
  ];

  //functions

  //app
  //app settings
  void changeThemeMode(bool isDark) {
    themeMode = isDark ? ThemeMode.dark : ThemeMode.light;
    emit(HomePageRefreshUIState());
  }

  void changeLanguage(bool isEnglish) {
    language = isEnglish ? "EN" : "AR";
    emit(HomePageRefreshUIState());
  }

  Map<String, String> selectLanguageMap() {
    if (language == "EN") {
      return englishMap;
    } else {
      return arabicMap;
    }
  }

  //app search
  void searchInApp(String value) {
    print(value);
    searchInAppList = productList
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    emit(HomePageRefreshUIState());
  }

  //app bottomNavigatorBar
  void setAppPageIndex(int index) {
    currentAppPageIndex = index;
    emit(HomePageRefreshUIState());
  }

  //app favorite
  void setIsFavorite(bool isFavorite, int index) {
    if (isFavorite) {
      productList[index].isFavorite = false;
      emit(HomePageRefreshUIState());
    } else {
      productList[index].isFavorite = true;
      emit(HomePageRefreshUIState());
    }
  }

  void setFavoriteList() {
    favoriteList =
        productList.where((element) => element.isFavorite == true).toList();
    emit(HomePageRefreshUIState());
  }

  //app on sale
  void setOnSaleList() {
    onSaleList =
        productList.where((element) => element.costPrice != 0.0).toList();
    emit(HomePageRefreshUIState());
  }

  //dashboard
  //dashboard bottomNavigatorBar
  void setDasboardPageIndex(int index) {
    currentDashboardPageIndex = index;
    emit(HomePageRefreshUIState());
  }

  //dashboard seach
  void searchCategoryInDashboard(String value) {
    searchCategoryList = categoriesList
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    emit(HomePageRefreshUIState());
  }

  void searchProductInDashboard(String value) {
    searchProductList = productList
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    emit(HomePageRefreshUIState());
  }

  void searchUserInDashboard(String value) {
    searchUserList = productList
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    emit(HomePageRefreshUIState());
  }

  //register
  Future<bool> register() async {
    try {
      var data = {
        "name": nameRegister.text,
        "phone": phoneRegister.text,
        "email": emailRegister.text,
        "password": passwordRegister.text,
      };
      var res = await DioHelper.dio!.post(
        "auth/register",
        data: jsonEncode(data),
      );
      if (res.statusCode == 200) {
        messagesRegister = res.data["message"];
        currentUser = UserModel.fromJson(res.data["user"]);
        return true;
      } else if (res.statusCode == 500) {
        messagesRegister = res.data["message"];
        return false;
      } else if (res.statusCode == 422) {
//         {
//     "success": false,
//     "message":"{\"email\":[\" \ي\ر\ج\ى \ا\د\خ\ا\ل \ح\س\ا\ب \ا\خ\ر\","uhuighsuigh"],
//                \"phone\":[\"\ا\ل\ر\ق\م \م\ط\ل\و\ب\"]
// }"
// }
        messagesRegister = "";
        Map<String, dynamic> messageMap = jsonDecode(res.data["message"]);
        for (var errors in messageMap.keys) {
          for (var error in messageMap[errors]) {
            messagesRegister += error;
            messagesRegister += "\n";
          }
        }
        return false;
      } else {
        return false;
      }
    } catch (e) {
      messagesRegister = e.toString();
      return false;
    }
  }

  //login
  Future<bool> login() async {
    try {
      var data = {
        "email": emailLogin.text.trim(),
        "password": passwordLogin.text.trim(),
      };
      var res = await DioHelper.dio!.post("auth/login", data: jsonEncode(data));
      if (res.statusCode == 200) {
        String userType = res.data["user_type"].toString();
        currentUser!.userType = UserTypeEnum.values[userType as int];
        messagesLogin = res.data["message"];
        await setTokenInSP(res.data["access_token"] as String);
        return true;
      } else if (res.statusCode == 422) {
//         {
//     "email": [
//         "هذا الحقل مطلوب"
//     ],
//     "password": [
//         "هذا الحقل مطلوب"
//     ]
// }
        for (var errors in res.data.keys) {
          for (var error in res.data[errors]) {
            messagesLogin += error;
            messagesLogin += "\n";
          }
        }
        await setTokenInSP("");
        return false;
      } else if (res.statusCode == 401) {
        messagesLogin = res.data["error"];
        return false;
      } else {
        return false;
      }
    } catch (e) {
      messagesLogin = e.toString();
      return false;
    }
  }

  Future<void> setTokenInSP(String tok) async {
    token = tok;
    await sp.setString("token", token ?? "");
    getTokenFromSP();
    emit(HomePageRefreshUIState());
  }

  void getTokenFromSP() {
    token = sp.getString("token");
    DioHelper.init(token: token ?? "");
    emit(HomePageRefreshUIState());
  }

  //verify
  Future<bool> verifyAccount() async {
    try {
//       {
//     "success": true,
//     "message": "User successfully verify"
// }
      var data = {
        "verified_code": currentUser!.verifiedCode,
      };
      int userId = currentUser!.id;
      var res = await DioHelper.dio!.post(
        "auth/verifiedAccount/$userId",
        data: jsonEncode(data),
      );
      if (res.statusCode == 200) {
        messagesVerify = res.data["message"];
        return true;
      } else {
        messagesVerify = res.data["verified_code"];
        return false;
      }
    } catch (e) {
      messagesVerify = e.toString();
      return false;
    }
  }

  Future<void> sendEmail() async {
    try {
      int userId = currentUser!.id;
      var response = await DioHelper.dio!.post(
        "auth/sendEmail/$userId",
      );
    } catch (e) {
      print(e);
    }
  }

  //profile
  Future<void> profile() async {
    try {
      int userId = currentUser!.id;
      var response = await DioHelper.dio!.post(
        "auth/auth/me/$userId",
      );
    } catch (e) {
      print(e);
    }
  }

  //logout
  Future<void> logout() async {
    try {
      var response = await DioHelper.dio!.post(
        "auth/logout",
      );
      await setTokenInSP("");
      messagesLogout = "تم تسجيل الخروج";
    } catch (e) {
      print(e);
    }
  }

  //categories
  Future<void> getAllCategories() async {
    try {
      isLoading = true;
      emit(HomePageRefreshUIState());
      Timer(const Duration(milliseconds: 2000), () {
        //   var response = await DioHelper.dio!.get(
        //   "groups/getAllCategories",
        // );
        Map<String, dynamic> response = {
          "data": [
            {
              "id": 0,
              "name": "رجالي",
              "description": "something",
              "iconPath":
                  "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Person_icon_BLACK-01.svg/962px-Person_icon_BLACK-01.svg.png",
            },
            {
              "id": 1,
              "name": "نسائي",
              "description": "something",
              "iconPath":
                  "https://cdn-icons-png.flaticon.com/512/103/103277.png",
            },
            {
              "id": 2,
              "name": "أطفال",
              "description": "something",
              "iconPath":
                  "https://media.istockphoto.com/id/1178790725/vector/children-icon.jpg?s=170667a&w=0&k=20&c=hfGALG3kbYHTbPThtZfW5TEVTLed8z6vvYnRAYy3lpM=",
            },
          ],
          "statusCode": 200,
        };
        if (response["statusCode"] == 200) {
          categoriesList.clear();
          for (var category in response["data"]) {
            categoriesList.add(CategoryModel.fromJson(category));
          }
        }
      });
      searchCategoryList = categoriesList;
    } catch (e) {
      isLoading = false;
      emit(HomePageRefreshUIState());
    }
  }

  Future<void> addCategory() async {
    try {
      CategoryModel categoryModel = CategoryModel(
        id: 0,
        name: categoryNameController.text.trim(),
      );
      isLoading = true;
      var res = await DioHelper.dio!.post(
        "categories/add",
        data: jsonEncode(
          categoryModel.toJson(),
        ),
      );
      if (res.statusCode == 200) {
        await getAllCategories();
        isLoading = false;
        emit(HomePageRefreshUIState());
      } else {
        isLoading = false;
        emit(HomePageRefreshUIState());
      }
    } catch (e) {
      isLoading = false;
      emit(HomePageRefreshUIState());
    }
  }

  Future<void> editCategory() async {
    try {
      CategoryModel categoryModel = CategoryModel(
        id: 0,
        name: categoryNameController.text.trim(),
      );
      isLoading = true;
      var res = await DioHelper.dio!.post(
        "categories/update/$currentCategoryId",
        data: jsonEncode(
          categoryModel.toJson(),
        ),
      );
      if (res.statusCode == 200) {
        await getAllCategories();
        isLoading = false;
        emit(HomePageRefreshUIState());
      } else {
        isLoading = false;
        emit(HomePageRefreshUIState());
      }
    } catch (e) {
      isLoading = false;
      emit(HomePageRefreshUIState());
    }
  }

  Future<void> deleteCategory(int id) async {
    try {
      isLoading = true;
      var res = await DioHelper.dio!.get(
        "categories/delete/$id",
      );
      if (res.statusCode == 200) {
        await getAllCategories();
        isLoading = false;
        emit(HomePageRefreshUIState());
      } else {
        isLoading = false;
        emit(HomePageRefreshUIState());
      }
    } catch (e) {
      isLoading = false;
      emit(HomePageRefreshUIState());
    }
  }

  void clearCategoryForm() {
    currentCategoryId = null;
    categoryNameController.text = "";
    categoryDescriptionController.text = "";
    categoryNotesController.text = "";
    emit(HomePageRefreshUIState());
  }

  void fillCategoryForm(CategoryModel categoryModel) {
    currentCategoryId = categoryModel.id;
    categoryNameController.text = categoryModel.name;
    categoryDescriptionController.text = categoryModel.description ?? "";
    categoryNotesController.text = categoryModel.notes ?? "";
    emit(HomePageRefreshUIState());
  }

  // void setCategoryImage(File image) {
  //   categoryImage = image;
  //   emit(HomePageRefreshUIState());
  // }

  //products
  Future<void> getAllProducts() async {
    try {
      Timer(const Duration(milliseconds: 2000), () {
        Map<String, dynamic> response = {
          "data": [
            {
              "id": 0,
              "name": "pants",
              "costPrice": 22000.00,
              "price": 20000.00,
              "isAvailable": true,
              "isFavorite": false,
              "categoryId": 0,
              "description": "Something about the product",
              "TVA": 0.0,
              "images": [
                {
                  "id": 0,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 1,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 2,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
              ],
            },
            {
              "id": 1,
              "name": "sweater",
              "price": 20000.00,
              "costPrice": 22000.00,
              "isAvailable": true,
              "isFavorite": false,
              "categoryId": 1,
              "description": "Something about the product",
              "TVA": 0.0,
              "images": [
                {
                  "id": 3,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 4,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 5,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
              ],
            },
            {
              "id": 2,
              "name": "pants",
              "price": 20000.00,
              "costPrice": 22000.00,
              "isAvailable": true,
              "isFavorite": false,
              "categoryId": 0,
              "description": "Something about the product",
              "TVA": 0.0,
              "images": [
                {
                  "id": 6,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 7,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
                {
                  "id": 8,
                  "image":
                      'https://media.istockphoto.com/id/1306307586/photo/collection-of-old-books-in-library.jpg?b=1&s=170667a&w=0&k=20&c=qEyK-d-aFOds4EcAYWoBIKnO5ELCiSb1PZpW_WU8RZ4=',
                },
              ],
            },
          ],
          "statusCode": 200,
        };
        if (response["statusCode"] == 200) {
          productList.clear();
          for (var product in response["data"]) {
            productList.add(ProductModel.fromJson(product));
          }
          isLoading = false;
          emit(HomePageRefreshUIState());
        }
      });
      searchInAppList = productList;
      searchProductList = productList;
    } catch (e) {
      isLoading = false;
      emit(HomePageRefreshUIState());
    }
  }

  void clearProductForm() {
    currentProductId = null;
    productNameController.text = "";
    productDescriptionController.text = "";
    productPriceController.text = "";
    categorySelectedModel = null;
    productNotesController.text = "";
    emit(HomePageRefreshUIState());
  }

  void fillProductForm(ProductModel productModel) {
    currentProductId = productModel.id;
    productNameController.text = productModel.name;
    categorySelectedModel = categoriesList
        .where((element) => element.id == productModel.categoryId)
        .first;
    productDescriptionController.text = productModel.description;
    productPriceController.text = productModel.price.toString();
    productNotesController.text = productModel.notes ?? "";

    emit(HomePageRefreshUIState());
  }

  Future<void> deleteProduct(int id) async {
    try {
      var res = await DioHelper.dio!.get(
        "products/delete/$id",
      );
      if (res.statusCode == 200) {
        await getAllProducts();
        emit(HomePageRefreshUIState());
      } else {
        emit(HomePageRefreshUIState());
      }
    } catch (e) {
      emit(HomePageRefreshUIState());
    }
  }

  void changeProductCategory(CategoryModel? categorySelectedModel) {
    this.categorySelectedModel = categorySelectedModel;
    emit(HomePageRefreshUIState());
  }

  //cart
  void addToCart(ProductModel productModel) {
    cartList.add(productModel);
    emit(HomePageRefreshUIState());
  }

  void removeFromCart(ProductModel productModel) {
    cartList.remove(productModel);
    emit(HomePageRefreshUIState());
  }
}
