import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/pages/main_my_home_page.dart';
import 'package:shop/settings/theme_context.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sp = await SharedPreferences.getInstance();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<HomePageCubit>(
          create: (context) => HomePageCubit(sp)
            ..getTokenFromSP()
            ..getAllCategories()
            ..getAllProducts(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: homePageCubit.themeMode,
          title: 'متجر',
          home: const MainMyHomePage(),
        );
      },
    );
  }
}
