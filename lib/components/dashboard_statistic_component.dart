import 'package:flutter/material.dart';

class DashboardStatisticComponent extends StatelessWidget {
  const DashboardStatisticComponent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Theme.of(context).colorScheme.secondary,
            Theme.of(context).colorScheme.primary,
          ],
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      child: Column(
        children: [
          IconButton(
            iconSize: 70,
            onPressed: () {},
            icon: Icon(
              Icons.people,
              color: Theme.of(context).colorScheme.surface,
            ),
          ),
          Text(
            "Users Numbers",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: Theme.of(context).colorScheme.surface,
            ),
          ),
          Text(
            "2000",
            style: TextStyle(
              color: Theme.of(context).colorScheme.surface,
            ),
          ),
        ],
      ),
    );
  }
}
