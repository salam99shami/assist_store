import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/pages/login_page.dart';

class ProductsGridTileComponent extends StatelessWidget {
  ProductsGridTileComponent({
    super.key,
    required this.productModel,
  });

  ProductModel productModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Container(
          width: MediaQuery.of(context).size.width * 0.5,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomLeft,
              colors: [
                Theme.of(context).colorScheme.primary,
                Theme.of(context).colorScheme.secondary,
              ],
            ),
            // color: Theme.of(context).colorScheme.primary,
            borderRadius: const BorderRadius.all(
              Radius.circular(12),
            ),
          ),
          child: GridTile(
            footer: GridTileBar(
              leading: IconButton(
                icon: const Icon(
                  Icons.add,
                ),
                onPressed: () {
                  if (homePageCubit.token == null) {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ),
                    );
                  } else {
                    homePageCubit.addToCart(productModel);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          languageMap["One item has been added"]!,
                        ),
                      ),
                    );
                  }
                },
              ),
              backgroundColor: Colors.black45,
              title: Text(
                productModel.name,
                textAlign: TextAlign.center,
              ),
              trailing: IconButton(
                icon: Icon(
                  Icons.favorite,
                  color: productModel.isFavorite!
                      ? Colors.red
                      : Theme.of(context).colorScheme.surface,
                ),
                onPressed: () {
                  homePageCubit.setIsFavorite(
                      productModel.isFavorite!, productModel.id);
                },
              ),
            ),
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                    top: 20,
                    bottom: 10,
                    left: 20,
                    right: 20,
                  ),
                  height: MediaQuery.of(context).size.height * 0.22,
                  width: MediaQuery.of(context).size.width * 0.5,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                        productModel.images![0].image,
                      ),
                      fit: BoxFit.fill,
                    ),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                ),
                // Text(
                //   "${productModel.costPrice} ل.س",
                //   style: TextStyle(
                //     decoration: TextDecoration.lineThrough,
                //     color: Theme.of(context).colorScheme.surface,
                //   ),
                // ),
                Text(
                  "${productModel.price} ل.س",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.surface,
                  ),
                ),
                // Wrap(
                //   crossAxisAlignment: WrapCrossAlignment.center,
                //   // alignment: WrapAlignment.center,
                //   textDirection: TextDirection.rtl,
                //   children: [
                //     Text(
                //       productModel.description,
                //       style: TextStyle(
                //         color: Theme.of(context).colorScheme.surface,
                //       ),
                //     ),
                //   ],
                // ),
              ],
            ),
          ),
        );
      },
    );
  }
}
