import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';

class AppBottomNavigatorBar extends StatelessWidget {
  const AppBottomNavigatorBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return BottomNavigationBar(
          // selectedFontSize: 11,
          selectedItemColor: Theme.of(context).colorScheme.surface,
          unselectedItemColor: Colors.black45,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const Icon(Icons.home),
              label: languageMap["Home"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.search),
              label: languageMap["Search"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.shopping_cart_checkout),
              label: languageMap["Cart"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.favorite),
              label: languageMap["Favorite"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              label: languageMap["Profile"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
          ],
          currentIndex: homePageCubit.currentAppPageIndex,
          onTap: (value) {
            homePageCubit.setAppPageIndex(value);
          },
        );
      },
    );
  }
}
