import 'package:flutter/material.dart';

class DropDownComponent<T> extends StatelessWidget {
  const DropDownComponent({
    Key? key,
    this.color,
    required this.items,
    required this.itemsBuilder,
    this.labeltext,
    this.value,
    this.focusNode,
    this.nullable = false,
    this.nullableText,
    this.onChanged,
    this.validator,
    this.keyFormRefresh,
  }) : super(key: key);

  final String? labeltext;
  final T? value;
  final Color? color;
  final bool nullable;
  final Function(T? newValue)? onChanged;
  final Widget Function(T? obj) itemsBuilder;
  final List<T>? items;
  final FocusNode? focusNode;
  final String? nullableText;
  final String? Function(T?)? validator;
  final GlobalKey<FormFieldState>? keyFormRefresh;

  DropdownMenuItem<T> itemBuilder(T o) {
    return DropdownMenuItem<T>(
      value: o,
      child: itemsBuilder(o),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<T>> dropDownItems = [];
    if (nullable) {
      dropDownItems.add(DropdownMenuItem(
        value: null,
        child: Text(nullableText ?? "غير محدد"),
      ));
    }

    items!.map((e) => dropDownItems.add(itemBuilder(e))).toList();
    Color? primaryColor = color ?? Theme.of(context).colorScheme.primary;
    return DropdownButtonFormField<T>(
      key: keyFormRefresh,
      focusNode: focusNode,
      decoration: InputDecoration(
        labelText: '$labeltext',
        labelStyle: TextStyle(color: primaryColor),
        border: OutlineInputBorder(
          //  borderRadius: BorderRadius.circular(50.0),
          borderSide: BorderSide(
            color: Colors.black.withOpacity(0.3),
            width: 1,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          //  borderRadius: BorderRadius.circular(50.0),
          borderSide: BorderSide(
            color: Colors.black.withOpacity(0.3),
            width: 1,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          //  borderRadius: BorderRadius.circular(50.0),
          borderSide: BorderSide(
            color: primaryColor,
            width: 2,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          //  borderRadius: BorderRadius.circular(50.0),
          borderSide: BorderSide(
            color: Colors.black.withOpacity(0.3),
            width: 1,
          ),
        ),
        contentPadding: const EdgeInsetsDirectional.only(
            start: 20, top: 20 / 2, bottom: 45 / 2, end: 45 * 0.60),
      ),
      icon: Icon(
        Icons.keyboard_arrow_down_sharp,
        color: color,
      ),
      value: value,
      onChanged: onChanged,
      validator: validator,
      items: dropDownItems,
      menuMaxHeight: 210,
    );
  }
}
