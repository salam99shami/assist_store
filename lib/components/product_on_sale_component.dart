import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/pages/product_detailed_page.dart';

class ProductOnSaleComponent extends StatelessWidget {
  const ProductOnSaleComponent({
    super.key,
    required this.productModel,
  });
  final ProductModel productModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Container(
          height: MediaQuery.of(context).size.height * 0.45,
          width: MediaQuery.of(context).size.width * 0.5,
          margin: EdgeInsets.only(
            left: 12,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomLeft,
              colors: [
                Theme.of(context).colorScheme.primary,
                Theme.of(context).colorScheme.secondary,
              ],
            ),
            borderRadius: const BorderRadius.all(
              Radius.circular(12),
            ),
          ),
          child: Column(
            children: [
              IconButton(
                alignment: Alignment(16, 0),
                icon: Icon(
                  Icons.favorite,
                  color: productModel.isFavorite!
                      ? Colors.red
                      : Theme.of(context).colorScheme.surface,
                ),
                onPressed: () {
                  homePageCubit.setIsFavorite(
                    productModel.isFavorite!,
                    productModel.id,
                  );
                },
              ),
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ProductDetailedPage(
                      productModel: productModel,
                    ),
                  ),
                ),
                child: Container(
                  width: 140,
                  height: 140,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                        productModel.images![0].image,
                      ),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                productModel.name,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Theme.of(context).colorScheme.surface,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                productModel.costPrice.toString() + " ل.س",
                style: TextStyle(
                  decoration: TextDecoration.lineThrough,
                  color: Theme.of(context).colorScheme.surface,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                productModel.price.toString() + " ل.س",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.surface,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
            ],
          ),
        );
      },
    );
  }
}
