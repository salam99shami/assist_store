import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/category_model.dart';
import 'package:shop/pages/dashboard_category_edit_page.dart';

class DashboardCategoryGridTileComponent extends StatelessWidget {
  DashboardCategoryGridTileComponent({
    super.key,
    required this.categoryModel,
  });

  CategoryModel categoryModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: GridTile(
            footer: GridTileBar(
              leading: IconButton(
                icon: const Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return Directionality(
                        textDirection: homePageCubit.language == "AR"
                            ? TextDirection.rtl
                            : TextDirection.ltr,
                        child: AlertDialog(
                          title: Text(languageMap["Are You Sure ?"]!),
                          actions: [
                            TextButton(
                              onPressed: () {
                                homePageCubit.deleteCategory(categoryModel.id);
                                Navigator.pop(context);
                              },
                              child: Text(languageMap["Yes"]!),
                            ),
                            TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(languageMap["No"]!),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
              ),
              backgroundColor: Colors.black26,
              title: Text(
                categoryModel.name,
                textAlign: TextAlign.center,
              ),
              trailing: IconButton(
                icon: const Icon(
                  Icons.edit,
                ),
                onPressed: () {
                  homePageCubit.fillCategoryForm(categoryModel);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DashboardCategoryEditPage(),
                    ),
                  );
                },
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    categoryModel.iconPath!,
                  ),
                  fit: BoxFit.fill,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
