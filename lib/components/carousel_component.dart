import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/image_model.dart';
import 'package:shop/models/product_model.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CarouselComponent extends StatefulWidget {
  CarouselComponent({
    super.key,
    required this.height,
    required this.controller,
    required this.dotHeight,
    required this.dotWidth,
    required this.productModel,
  });
  final CarouselController controller;
  final double height;
  final double dotHeight;
  final double dotWidth;
  ProductModel productModel;

  @override
  State<CarouselComponent> createState() => _CarouselComponentState();
}

int activeIndex = 0;

class _CarouselComponentState extends State<CarouselComponent> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
        listener: (context, state) {},
        builder: (context, state) {
          HomePageCubit homePageCubit = HomePageCubit.get(context);
          return Stack(alignment: Alignment.center, children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width * 0.9,
              clipBehavior: Clip.antiAlias,
              //  حتى الابن ياخد خصائص الأب
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.surface,
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
              child: CarouselSlider(
                carouselController: widget.controller,
                items: widget.productModel.images!
                    .map(
                      (item) => Image.network(
                        item.image,
                        fit: BoxFit.cover,
                      ),
                    )
                    .toList(),
                options: CarouselOptions(
                  autoPlayInterval: const Duration(seconds: 2),
                  aspectRatio: 1,
                  enlargeCenterPage: true,
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {
                    setState(() {
                      activeIndex = index;
                    });
                  },
                ),
              ),
            ),
            Positioned(
              right: 8,
              top: 8,
              child: IconButton(
                iconSize: 40,
                icon: Icon(
                  Icons.favorite,
                  color: widget.productModel.isFavorite!
                      ? Colors.red
                      : Theme.of(context).colorScheme.onPrimary,
                ),
                onPressed: () {
                  homePageCubit.setIsFavorite(
                      widget.productModel.isFavorite!, widget.productModel.id);
                },
              ),
            ),
            Positioned(
              bottom: 10,
              child: AnimatedSmoothIndicator(
                activeIndex: activeIndex,
                count: widget.productModel.images!.length,
                effect: SlideEffect(
                  dotColor: Colors.white,
                  activeDotColor: Theme.of(context).colorScheme.primary,
                  dotHeight: widget.dotHeight,
                  dotWidth: widget.dotWidth,
                ),
                onDotClicked: (index) {
                  widget.controller.animateToPage(index);
                },
              ),
            ),
          ]);
        });
  }
}
