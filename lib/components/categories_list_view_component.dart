import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/category_component.dart';

class CategoryListViewComponent extends StatelessWidget {
  CategoryListViewComponent({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        return Container(
          margin: EdgeInsets.only(
            right: 2,
          ),
          height: 110,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: homePageCubit.categoriesList.length,
            itemBuilder: ((context, index) {
              return CategoryComponent(
                category: homePageCubit.categoriesList[index],
              );
            }),
          ),
        );
      },
    );
  }
}
