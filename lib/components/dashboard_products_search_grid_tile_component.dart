import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/pages/dashboard_product_edit_page.dart';

class DashboardProductsSearchGridTileComponent extends StatelessWidget {
  DashboardProductsSearchGridTileComponent({
    super.key,
    required this.productModel,
  });

  ProductModel productModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridTile(
            footer: GridTileBar(
              leading: IconButton(
                icon: const Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(languageMap["Are You Sure ?"]!),
                        actions: [
                          TextButton(
                            onPressed: () {
                              homePageCubit.deleteCategory(productModel.id);
                              Navigator.pop(context);
                            },
                            child: const Text("Yes"),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text("No"),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
              backgroundColor: Colors.black26,
              title: Text(
                productModel.name,
                textAlign: TextAlign.center,
              ),
              trailing: IconButton(
                icon: const Icon(
                  Icons.edit,
                ),
                onPressed: () {
                  // homePageCubit.fillProductForm(productModel);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DashboardProductEditPage(),
                    ),
                  );
                },
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    productModel.images![0].image,
                  ),
                  fit: BoxFit.fill,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
