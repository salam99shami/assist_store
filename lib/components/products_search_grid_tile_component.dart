import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/pages/login_page.dart';

class ProductsSearchGridTileComponent extends StatelessWidget {
  ProductsSearchGridTileComponent({
    super.key,
    required this.productModel,
  });

  ProductModel productModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: GridTile(
            footer: GridTileBar(
              leading: IconButton(
                icon: const Icon(
                  Icons.add,
                ),
                onPressed: () {
                  if (homePageCubit.token == null) {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ),
                    );
                  }
                },
              ),
              backgroundColor: Colors.black26,
              title: Text(
                productModel.name,
                textAlign: TextAlign.center,
              ),
              trailing: IconButton(
                icon: Icon(
                  Icons.favorite,
                  color: productModel.isFavorite!
                      ? Colors.red
                      : Theme.of(context).colorScheme.surface,
                ),
                onPressed: () {
                  homePageCubit.setIsFavorite(
                      productModel.isFavorite!, productModel.id);
                },
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    productModel.images![0].image,
                  ),
                  fit: BoxFit.fill,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
