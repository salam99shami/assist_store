import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shop/models/product_model.dart';
import 'package:shop/pages/product_detailed_page.dart';

class ProductComponent extends StatelessWidget {
  const ProductComponent({
    super.key,
    required this.productModel,
    required this.controller,
  });
  final ProductModel productModel;
  final CarouselController controller;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ProductDetailedPage(
              productModel: productModel,
            ),
          ),
        );
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        shadowColor: Theme.of(context).colorScheme.primary,
        elevation: 2,
        child: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(
                right: 10,
              ),
              width: 140,
              height: 140,
              decoration: BoxDecoration(
                boxShadow: [
                  const BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 0.5,
                    blurRadius: 0.5,
                  ),
                ],
                image: DecorationImage(
                  image: NetworkImage(
                    productModel.images![0].image,
                  ),
                  fit: BoxFit.fill,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                right: 10,
              ),
              height: MediaQuery.of(context).size.height * 0.3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    productModel.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  const SizedBox(height: 12),
                  // Text(
                  //   "${productModel.costPrice} ل.س",
                  //   style: const TextStyle(
                  //     decoration: TextDecoration.lineThrough,
                  //   ),
                  // ),
                  // const SizedBox(height: 12),
                  Text(
                    "${productModel.price} ل.س",
                  ),
                  const SizedBox(height: 12),
                  Text(
                    productModel.description,
                    style: const TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
