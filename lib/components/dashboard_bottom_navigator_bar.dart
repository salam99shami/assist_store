import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';

class DashboardBottomNavigatorBar extends StatelessWidget {
  const DashboardBottomNavigatorBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        Map<String, String> languageMap = homePageCubit.selectLanguageMap();
        return BottomNavigationBar(
          selectedItemColor: Theme.of(context).colorScheme.surface,
          unselectedItemColor: Colors.black45,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const Icon(Icons.dashboard),
              label: languageMap["Dashboard"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.category),
              label: languageMap["Categories"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.manage_accounts),
              label: languageMap["Manage Accounts"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.production_quantity_limits),
              label: languageMap["Products"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              label: languageMap["Profile"]!,
              backgroundColor: Theme.of(context).colorScheme.primary,
            ),
          ],
          currentIndex: homePageCubit.currentDashboardPageIndex,
          onTap: (value) {
            homePageCubit.setDasboardPageIndex(value);
          },
        );
      },
    );
  }
}
