import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/product_on_sale_component.dart';

class ProductOnSaleListViewComponent extends StatelessWidget {
  ProductOnSaleListViewComponent({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        return Container(
          margin: const EdgeInsets.only(
            right: 10,
            left: 10,
          ),
          height: MediaQuery.of(context).size.height * 0.45,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: homePageCubit.productList.length,
            itemBuilder: ((context, index) {
              return ProductOnSaleComponent(
                productModel: homePageCubit.productList[index],
              );
            }),
          ),
        );
      },
    );
  }
}
