import 'package:flutter/material.dart';
import 'package:shop/models/category_model.dart';
import 'package:shop/pages/sub_categories_page.dart';

class CategoryComponent extends StatelessWidget {
  const CategoryComponent({
    super.key,
    required this.category,
  });

  final CategoryModel category;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => SubCategoriesPage(categoryModel: category),
        ),
      ),
      child: Container(
        width: 100,
        margin: const EdgeInsets.all(6),
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).colorScheme.primary,
              spreadRadius: 0.1,
              blurRadius: 0.5,
              // offset: const Offset(0.5, 1),
            ),
          ],
        ),
        child: Column(
          children: [
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    category.iconPath!,
                  ),
                  fit: BoxFit.cover,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
            ),
            Text(category.name),
          ],
        ),
      ),
    );
  }
}
