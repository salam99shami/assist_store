import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/components/product_component.dart';

import '../bloc/cubits/home_page_cubit.dart';
import '../bloc/states/home_page_states.dart';

class ProductsListViewComponent extends StatelessWidget {
  ProductsListViewComponent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        return Container(
          height: MediaQuery.of(context).size.height * 1,
          width: MediaQuery.of(context).size.width * 1,
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemCount: homePageCubit.productList.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 4.0,
                  vertical: 4.0,
                ),
                child: ProductComponent(
                  productModel: homePageCubit.productList[index],
                  controller: homePageCubit.controller,
                ),
              );
            },
          ),
        );
      },
    );
  }
}
