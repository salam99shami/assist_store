import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop/bloc/cubits/home_page_cubit.dart';
import 'package:shop/bloc/states/home_page_states.dart';
import 'package:shop/components/products_grid_tile_component.dart';

class ProductGridViewComponent extends StatelessWidget {
  ProductGridViewComponent({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomePageCubit, HomePageStates>(
      listener: (context, state) {},
      builder: (context, state) {
        HomePageCubit homePageCubit = HomePageCubit.get(context);
        return Container(
          margin: const EdgeInsets.only(
            right: 8,
            left: 8,
          ),
          width: MediaQuery.of(context).size.width * 1,
          height: MediaQuery.of(context).size.height * 1,
          child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              childAspectRatio: 0.68,
            ),
            // shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: homePageCubit.searchInAppList.length,
            itemBuilder: ((context, index) {
              return ProductsGridTileComponent(
                productModel: homePageCubit.searchInAppList[index],
              );
            }),
          ),
        );
      },
    );
  }
}
